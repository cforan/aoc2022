use std::io::{Read, Result as IOResult};
use std::error::Error;
use std::str::FromStr;
use std::time::{Duration, Instant};

fn main() -> Result<(), Box<dyn Error>> {

	let input = get_input()?;

	let (p1_result, p1_time) = part1(input.clone())?;
	println!("got {} for part 1 in {}us", p1_result, p1_time.as_micros());

	let (p2_result, p2_time) = part2(input.clone())?;
	println!("got {} for part 2 in {}us", p2_result, p2_time.as_micros());

	Ok(())
}

fn get_input() -> IOResult<String> {

	let mut buf = String::new();

	std::io::stdin().lock().read_to_string(&mut buf)?;

	Ok(buf)
}

fn part1(input: String) -> Result<(usize, Duration), Box<dyn Error>> {

	let start = Instant::now();

	let forest = Forest::from_str(&input)?;

	let (width, height) = forest.size();

	let north_visibility = VisibilityKernel::new(width, height, Direction::North).convolve(&forest);
	let east_visibility  = VisibilityKernel::new(width, height, Direction::East).convolve(&forest);
	let south_visibility = VisibilityKernel::new(width, height, Direction::South).convolve(&forest);
	let west_visibility  = VisibilityKernel::new(width, height, Direction::West).convolve(&forest);

	let visibility: Vec<i32> = north_visibility.iter()
		.zip(east_visibility.iter())
		.map(|(n, e)| n.min(e))
		.zip(south_visibility.iter())
		.map(|(ne, s)| ne.min(s))
		.zip(west_visibility.iter())
		.map(|(nes, w)| nes.min(w).to_owned())
		.collect();

	let result = visibility.iter()
		.filter(|&v| *v == 0)
		.count();

	Ok((result, Instant::now() - start))
}

fn part2(input: String) -> Result<(i32, Duration), Box<dyn Error>> {

	let start = Instant::now();

	let forest = Forest::from_str(&input)?;

	let (width, height) = forest.size();

	let north_scenic = ScenicKernel::new(width, height, Direction::North).convolve(&forest);
	let east_scenic  = ScenicKernel::new(width, height, Direction::East).convolve(&forest);
	let south_scenic = ScenicKernel::new(width, height, Direction::South).convolve(&forest);
	let west_scenic  = ScenicKernel::new(width, height, Direction::West).convolve(&forest);

	let scenic_score: i32 = north_scenic.iter()
		.zip(east_scenic.iter())
		.map(|(n, e)| n * e)
		.zip(south_scenic.iter())
		.map(|(ne, s)| ne * s)
		.zip(west_scenic.iter())
		.map(|(nes, w)| nes * w)
		.max()
		.unwrap_or(0);

	Ok((scenic_score, Instant::now() - start))
}

trait Kernel<T, C>
where
	T: Default + Clone + std::ops::Mul<Output=T>,
	C: ConvolutionTarget<T>
{
	fn convolve(&self, src: &C) -> Vec<T> {

		let neigh = self.neigh();

		let (width, height) = src.size();

		let mut result = vec![T::default(); width * height];

		for y in 0..height {
			for x in 0..width {

				result[y * width + x] = Self::process(&src.get(x as isize, y as isize), &src.get_neigh(&neigh, x as isize, y as isize));
			}
		}

		result
	}
	fn process(this: &T, neigh: &Vec<T>) -> T;
	fn neigh(&self) -> &Vec<(isize, isize, T)>;
}

trait ConvolutionTarget<T>
where T: Default + Clone + std::ops::Mul<Output=T> {
	fn size(&self) -> (usize, usize);
	fn get(&self, x: isize, y: isize) -> T;
	fn get_neigh(&self, neigh: &[(isize, isize, T)], x: isize, y: isize) -> Vec<T> {

		let (w, h) = self.size();

		neigh.iter()
			.filter_map(|(nx, ny, f)| {
				let (lx, ly) = (x+nx, y+ny);
				if lx < 0 || lx >= w as isize || ly < 0 || ly >= h as isize {
					None
				} else {
					Some((lx, ly, f))
				}
			})
			.map(|(lx, ly, f)| self.get(lx, ly).to_owned() * f.to_owned())
			.collect::<Vec<T>>()
	}
}

enum Direction {
	North,
	East,
	South,
	West
}

struct VisibilityKernel {
	neigh: Vec<(isize, isize, i32)>
}

impl VisibilityKernel {

	pub fn new(width: usize, height: usize, dir: Direction) -> Self {
		
		let mut neigh = Vec::with_capacity(usize::max(width, height));
		
		let (width, height) = (width as isize, height as isize);

		match dir {
			Direction::North => {
				for y in (-(height-1)..0).rev() {
					neigh.push((0, y, 1));
				}
			},
			Direction::East => {
				for x in 1..width {
					neigh.push((x, 0, 1));
				}
			},
			Direction::South => {
				for y in 1..height {
					neigh.push((0, y, 1));
				}
			},
			Direction::West => {
				for x in (-(width-1)..0).rev() {
					neigh.push((x, 0, 1));
				}
			}
		}

		Self {
			neigh
		}
	}
}

impl Kernel<i32, Forest> for VisibilityKernel {

	fn neigh(&self) -> &Vec<(isize, isize, i32)> {
		&self.neigh
	}

	fn process(this: &i32, neigh: &Vec<i32>) -> i32 {

		neigh.iter()
			.filter(|&i| this <= i)
			.count() as i32
	}
}

struct ScenicKernel {
	neigh: Vec<(isize, isize, i32)>
}

impl ScenicKernel {

	pub fn new(width: usize, height: usize, dir: Direction) -> Self {

		Self {
			// steal this impl rather than write again
			neigh: VisibilityKernel::new(width, height, dir).neigh
		}
	}
}

impl Kernel<i32, Forest> for ScenicKernel {

	fn neigh(&self) -> &Vec<(isize, isize, i32)> {
		&self.neigh
	}

	fn process(this: &i32, neigh: &Vec<i32>) -> i32 {
		
		let mut iter = neigh.iter();
		let mut count: i32 = 0;

		while let Some(h) = iter.next() {

			count += 1;
			
			if h >= this {
				break;
			}
		}

		count
	}
}

#[derive(Debug)]
struct ParseForestError;

impl Error for ParseForestError {}

impl std::fmt::Display for ParseForestError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "Failed to parse input")
	}
}

#[derive(Debug)]
struct Forest {
	trees: Vec<i32>,
	width: usize,
	height: usize
}

impl FromStr for Forest {

	type Err = ParseForestError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		
		let lines_iter = s.split('\n');
		
		let mut line_length = 0;
		let mut num_lines = 0;
		let mut trees = vec![];

		for line in lines_iter {

			if line.is_empty() { break; }

			if line_length < 1 {
				line_length = line.len();
			}

			line.chars().for_each(|c| trees.push(c as i32 - '0' as i32));
			num_lines += 1;
		}

		Ok(Self {
			trees,
			width: line_length,
			height: num_lines
		})
	}
}

impl ConvolutionTarget<i32> for Forest {

	fn size(&self) -> (usize, usize) {
		(self.width, self.height)
	}

	fn get(&self, x: isize, y: isize) -> i32 {
		
		if x < 0 || x >= self.width as isize || y < 0 || y >= self.height as isize {
			-1
		} else {
			self.trees.get(y as usize * self.width + x as usize).map(|i| i.to_owned()).unwrap_or(-1)
		}
	}
}