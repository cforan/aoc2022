use std::io::{Read, Result as IOResult};
use std::error::Error;
use std::time::Instant;

fn main() -> Result<(), Box<dyn Error>> {

	let input = read_input()?;

	part_1(&input)?;
	part_2(&input)?;
	
	Ok(())
}

fn part_1(input_lines: &Vec<String>) -> Result<(), Box<dyn Error>> {

	let start = Instant::now();

	let result: i32 = input_lines.iter().map(|line| {

		let mut counts_comp1 = [0i32; 52];
		let mut counts_comp2 = [0i32; 52];

		let chars: Vec<char> = line.chars().collect();

		let len = line.len();

		for i in 0..(len/2) {

			counts_comp1[(chars[i].priority()-1) as usize] += 1;
			counts_comp2[(chars[i+len/2].priority()-1) as usize] += 1;
		}

		let mut line_priority = 0i32;

		for (i, (&c1, &c2)) in counts_comp1.iter().zip(counts_comp2.iter()).enumerate() {

			if c1 > 0 && c2 > 0 {
				line_priority += (i+1) as i32;
			}
		}

		line_priority
	}).sum();

	println!("got {} in {} microseconds for part 1", result, (Instant::now() - start).as_micros());

	Ok(())
}

fn part_2(input_lines: &Vec<String>) -> Result<(), Box<dyn Error>> {

	let start = Instant::now();

	let mut lines_iter = input_lines.iter();

	let mut result: i32 = 0;

	loop {

		let b1 = lines_iter.next();
		let b2 = lines_iter.next();
		let b3 = lines_iter.next();

		if b1.is_none() || b2.is_none() || b3.is_none() {
			break;
		}

		let mut counts_b1 = [0i32; 52];
		let mut counts_b2 = [0i32; 52];
		let mut counts_b3 = [0i32; 52];

		b1.unwrap().chars().for_each(|c| counts_b1[c.priority() as usize - 1] += 1);
		b2.unwrap().chars().for_each(|c| counts_b2[c.priority() as usize - 1] += 1);
		b3.unwrap().chars().for_each(|c| counts_b3[c.priority() as usize - 1] += 1);

		let mut priority = 0i32;

		for (i, ((&c1, &c2), &c3)) in counts_b1.iter().zip(counts_b2.iter()).zip(counts_b3.iter()).enumerate() {

			if c1 > 0 && c2 > 0 && c3 > 0 {

				priority += (i+1) as i32;
			}
		}

		result += priority;
	}

	println!("got {} in {} microseconds for part 2", result, (Instant::now() - start).as_micros());

	Ok(())
}

fn read_input() -> IOResult<Vec<String>> {

	let mut buf = String::new();

	std::io::stdin().lock().read_to_string(&mut buf)?;

	let r: Vec<String> = buf.split('\n').map(|s| s.to_owned()).collect();

	Ok(r)
}

pub trait Priority {
	fn priority(self) -> i32;
}

impl Priority for char {

	#[inline(always)]
	fn priority(self) -> i32 {
		
		if self >= 'a' && self <= 'z' {
			(self as i32 - 'a' as i32) + 1
		} else if self >= 'A' && self <= 'Z' {
			(self as i32 - 'A' as i32) + 27
		} else {
			0
		}
	}
}