use std::io::{Result as IOResult, Read};
use std::error::Error;
use std::time::Instant;
use std::str::FromStr;
use std::cmp::Ordering;


fn main() -> Result<(), Box<dyn Error>> {

	let input = read_input()?;

	part_1(&input)?;

	part_2(&input)?;

	Ok(())
}

fn part_1(input_lines: &Vec<String>) -> Result<(), Box<dyn Error>> {

	let start = Instant::now();

	let result: i32 = input_lines.iter()
		.map(|s| {
			let mut splitline = s.split(" ").take(2);
			(splitline.next(), splitline.next())
		})
		.filter(|(opp, pl)| opp.is_some() && pl.is_some())
		.map(|(opp, pl)| (Shape::from_str(opp.unwrap()), Shape::from_str(pl.unwrap())))
		.filter(|(opp, pl)| opp.is_ok() && pl.is_ok())
		.map(|(opp, pl)| (opp.unwrap(), pl.unwrap()))
		.map(|(opp, pl)| <GameResult as Into<i32>>::into(GameResult::from((pl, opp))) + <Shape as Into<i32>>::into(pl))
		.sum();

	println!("got {} in {} microseconds for part 1", result, (Instant::now() - start).as_micros());

	Ok(())
}

fn part_2(input_lines: &Vec<String>) -> Result<(), Box<dyn Error>> {

	let start = Instant::now();

	let result: i32 = input_lines.iter()
		.map(|s| {
			let mut splitline = s.split(" ").take(2);
			(splitline.next(), splitline.next())
		})
		.filter(|(opp, res)| opp.is_some() && res.is_some())
		.map(|(opp, res)| (Shape::from_str(opp.unwrap()), GameResult::from_str(res.unwrap())))
		.filter(|(opp, res)| opp.is_ok() && res.is_ok())
		.map(|(opp, res)| (opp.unwrap(), res.unwrap()))
		.map(|(opp, res)| <GameResult as Into<i32>>::into(res) + <Shape as Into<i32>>::into(res.move_for(opp)))
		.sum();

	println!("got {} in {} microseconds for part 2", result, (Instant::now() - start).as_micros());

	Ok(())
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum GameResult {
	Win,
	Lose,
	Draw
}

impl From<(Shape, Shape)> for GameResult {
	fn from(game: (Shape, Shape)) -> Self {

		match game.0.cmp(&game.1) {
			Ordering::Greater => Self::Win,
			Ordering::Less => Self::Lose,
			Ordering::Equal => Self::Draw
		}
	}
}

impl FromStr for GameResult {

	type Err = ParseError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {

		match s.to_uppercase().chars().next().ok_or(ParseError::EmptyString)? {
			'X' => Ok(GameResult::Lose),
			'Y' => Ok(GameResult::Draw),
			'Z' => Ok(GameResult::Win),
			_ => Err(ParseError::InvalidChar)
		}
	}
}

impl Into<i32> for GameResult {
	fn into(self) -> i32 {
		match self {
			Self::Win => 6,
			Self::Lose => 0,
			Self::Draw => 3
		}
	}	
}

impl GameResult {

	pub fn move_for(self, shape: Shape) -> Shape {

		use GameResult::*;
		use Shape::*;

		match self {
			Win => match shape {
				Rock => Paper,
				Paper => Scissors,
				Scissors => Rock
			},
			Draw => shape,
			Lose => match shape {
				Rock => Scissors,
				Paper => Rock,
				Scissors => Paper
			}
		}
	}
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum Shape {
	Rock,
	Paper,
	Scissors
}

impl PartialOrd for Shape {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		if self == other {
			Some(Ordering::Equal)
		} else if *self == Self::Rock && *other == Self::Scissors
		|| *self == Self::Paper && *other == Self::Rock
		|| *self == Self::Scissors && *other == Self::Paper {
			Some(Ordering::Greater)
		} else {
			Some(Ordering::Less)
		}
	}
}

impl Ord for Shape {
	fn cmp(&self, other: &Self) -> Ordering {
		self.partial_cmp(other).unwrap()
	}
}

impl Into<i32> for Shape {
	fn into(self) -> i32 {
		match self {
			Self::Rock     => 1,
			Self::Paper    => 2,
			Self::Scissors => 3
		}
	}
}

impl FromStr for Shape {

	type Err = ParseError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {

		match s.to_uppercase().chars().next().ok_or(ParseError::EmptyString)? {
			'A' | 'X' => Ok(Shape::Rock),
			'B' | 'Y' => Ok(Shape::Paper),
			'C' | 'Z' => Ok(Shape::Scissors),
			_ => Err(ParseError::InvalidChar)
		}
	}
}

impl Shape {

	pub fn parse_from_line(l: &str) -> Result<(Shape, Shape), ParseError> {

		let mut input = l.split(' ').take(2);

		Ok((
			Shape::from_str(input.next().unwrap_or(""))?,
			Shape::from_str(input.next().unwrap_or(""))?)
		)
	}
}

#[derive(Debug)]
pub enum ParseError {
	EmptyString,
	InvalidChar
}

impl Error for ParseError {}

impl std::fmt::Display for ParseError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", match self {
			Self::EmptyString => "Empty String",
			Self::InvalidChar => "Invalid Char"
		})
	}
}

fn read_input() -> IOResult<Vec<String>> {

	let mut buf = String::new();

	std::io::stdin().lock().read_to_string(&mut buf)?;

	let r: Vec<String> = buf.split('\n').map(|s| s.to_owned()).collect();

	Ok(r)
}