use std::error::Error;
use std::io::{Result as IOResult, Read};
use std::time::{Duration, Instant};


fn main() -> Result<(), Box<dyn Error>> {

	let input = read_input()?;

	let (p1_result, p1_time) = part_1(input.clone())?;
	println!("got {} in {} microseconds for part 1", p1_result, p1_time.as_micros());

	let (p2_result, p2_time) = part_2(input.clone())?;
	println!("got {} in {} microseconds for part 1", p2_result, p2_time.as_micros());


	Ok(())
}

fn part_1(input: String) -> Result<(usize, Duration), Box<dyn Error>> {

	let start = Instant::now();

	let input_chars: Vec<char> = input.chars().collect();

	let mut char_counts = [0usize; 26];

	let mut i: usize = 4;

	loop {

		if i >= input.len() { break; }

		for ci in (i - 4)..i {
			char_counts[input_chars[ci] as usize - 'a' as usize] += 1;
		}

		if !char_counts.iter().any(|c| c > &1) { break; }

		i += 1;

		char_counts.iter_mut().for_each(|i| *i = 0);
	}

	Ok((i, Instant::now() - start))
}


fn part_2(input: String) -> Result<(usize, Duration), Box<dyn Error>> {

	let start = Instant::now();

	let input_chars: Vec<char> = input.chars().collect();

	let mut char_counts = [0usize; 26];

	let mut i: usize = 14;

	loop {

		if i >= input.len() { break; }

		for ci in (i - 14)..i {
			char_counts[input_chars[ci] as usize - 'a' as usize] += 1;
		}

		if !char_counts.iter().any(|c| c > &1) { break; }

		i += 1;

		char_counts.iter_mut().for_each(|i| *i = 0);
	}

	Ok((i, Instant::now() - start))
}


fn read_input() -> IOResult<String> {

	let mut buf = String::new();

	std::io::stdin().lock().read_to_string(&mut buf)?;

	Ok(buf)
}
