use std::io::BufRead;
use std::error::Error;
use std::io::Result as IOResult;

fn main() -> Result<(), Box<dyn Error>> {
	
	let lines = read_input()?;

	let mut elves: Vec<i32> = vec![];

	let mut elf = 0;

	for line in lines {

		if let Ok(cal) = line.parse::<i32>() {
			elf += cal;
		} else {
			elves.push(elf);
			elf = 0;
		}
	}

	if elf != 0 {
		elves.push(elf);
	}

	println!("{}", elves.iter().max().unwrap_or(&0));

	Ok(())
}

fn read_input() -> IOResult<Vec<String>> {

	let mut lines = vec![];

	let stdin = std::io::stdin();

	let mut buf = String::new();

	loop {

		buf.clear();
		if stdin.lock().read_line(&mut buf)? == 0 {
			break;
		}

		lines.push(buf.trim().clone().to_owned());
	}

	Ok(lines)
}