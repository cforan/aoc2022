use std::io::{Result as IOResult, Read};
use std::error::Error;
use std::time::{Instant, Duration};
use std::ops::RangeInclusive;

fn main() -> Result<(), Box<dyn Error>> {
	
	let input = read_input()?;

	let (part1_result, part1_time) = part1(&input)?;
	println!("got {} for part 1 in {}us", part1_result, part1_time.as_micros());

	let (part2_result, part2_time) = part2(&input)?;
	println!("got {} for part 2 in {}us", part2_result, part2_time.as_micros());

	Ok(())
}

fn part1(input_lines: &Vec<String>) -> Result<(i32, Duration), Box<dyn Error>> {

	let start = Instant::now();

	let result: i32 = input_lines.iter()
		.map(|l| {
			let mut elves = l.split(',')
				.map(|e| {
					let mut range_iter = e.split('-')
						.filter_map(|n| n.parse::<i32>().ok());
				
					(range_iter.next(), range_iter.next())
				})
				.filter(|(r1, r2)| r1.is_some() && r2.is_some())
				.map(|(r1, r2)| (r1.unwrap()..=r2.unwrap()));

			let elf1 = elves.next();
			let elf2 = elves.next();
			(elf1, elf2)
		})
		.filter(|(e1, e2)| e1.is_some() && e2.is_some())
		.map(|(e1, e2)| (e1.unwrap(), e2.unwrap()))
		.filter(|(e1, e2)| e1.contains_all(e2) || e2.contains_all(e1))
		.count() as i32;

	Ok((result, Instant::now() - start))
}

fn part2(input_lines: &Vec<String>) -> Result<(i32, Duration), Box<dyn Error>> {

	let start = Instant::now();

	let result: i32 = input_lines.iter()
		.map(|l| {
			let mut elves = l.split(',')
				.map(|e| {
					let mut range_iter = e.split('-')
						.filter_map(|n| n.parse::<i32>().ok());
				
					(range_iter.next(), range_iter.next())
				})
				.filter(|(r1, r2)| r1.is_some() && r2.is_some())
				.map(|(r1, r2)| (r1.unwrap()..=r2.unwrap()));

			let elf1 = elves.next();
			let elf2 = elves.next();
			(elf1, elf2)
		})
		.filter(|(e1, e2)| e1.is_some() && e2.is_some())
		.map(|(e1, e2)| (e1.unwrap(), e2.unwrap()))
		.filter(|(e1, e2)| e1.overlaps(e2) || e2.overlaps(e1))
		.count() as i32;

	Ok((result, Instant::now() - start))
}

fn read_input() -> IOResult<Vec<String>> {

	let mut buf = String::new();

	std::io::stdin().lock().read_to_string(&mut buf)?;

	let r: Vec<String> = buf.split('\n').map(|s| s.to_owned()).collect();

	Ok(r)
}

pub trait ContainsAll {
	fn contains_all(&self, other: &Self) -> bool;
}

impl <T> ContainsAll for RangeInclusive<T>
where T: Ord
{
	fn contains_all(&self, other: &Self) -> bool {
		other.start() >= self.start() && other.end() <= self.end()
	}
}

pub trait Overlaps {
	fn overlaps(&self, other: &Self) -> bool;
}

impl <T> Overlaps for RangeInclusive<T>
where T: Ord
{
	fn overlaps(&self, other: &Self) -> bool {
		self.contains(other.start()) || self.contains(other.end())
	}
}