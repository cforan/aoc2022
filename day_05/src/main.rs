use std::io::Read;
use std::error::Error;
use std::time::{Duration, Instant};

type Input = (Vec<Vec<char>>, Vec<(u16, u16, u16)>);

fn main() -> Result<(), Box<dyn Error>> {

	let input = get_input()?;

	let (p1_result, p1_time) = part1(input.clone())?;
	println!("got {} for part 1 in {}us", p1_result, p1_time.as_micros());

	let (p2_result, p2_time) = part2(input.clone())?;
	println!("got {} for part 2 in {}us", p2_result, p2_time.as_micros());

	Ok(())
}

fn part1(input: Input) -> Result<(String, Duration), Box<dyn Error>> {

	let (mut crate_stacks, instructions) = input;

	let start = Instant::now();

	for (num, from, to) in instructions.iter() {

		crate_stacks.move_crates(*num, *from as usize - 1, *to as usize - 1);
	}

	let result = crate_stacks.top_each().iter().filter_map(|c| *c).collect::<String>();

	Ok((result, Instant::now() - start))
}

fn part2(input: Input) -> Result<(String, Duration), Box<dyn Error>> {

	let (mut crate_stacks, instructions) = input;

	let start = Instant::now();

	for (num, from, to) in instructions.iter() {

		crate_stacks.move_crates_at_once(*num, *from as usize - 1, *to as usize - 1);
	}

	let result = crate_stacks.top_each().iter().filter_map(|c| *c).collect::<String>();

	Ok((result, Instant::now() - start))
}


fn get_input() -> Result<Input, Box<dyn Error>> {

	let mut buf = String::new();

	std::io::stdin().lock().read_to_string(&mut buf)?;

	let (crates_str, instructions_str) = {
		let mut s = buf.split("\n\n");
		(s.next().unwrap(), s.next().unwrap())
	};

	let mut crate_stacks: Vec<Vec<char>> = vec![vec![]; 0];

	crates_str.split('\n').for_each(|crates_row| {

		let chars: Vec<char> = crates_row.chars().collect();
		let mut i = 1;
		let mut ci = 0;
		while i < chars.len() {

			while crate_stacks.len() <= ci {
				crate_stacks.push(vec![]);
			}

			chars.get(i).map(|c| {
				if let Some(cs) = crate_stacks.get_mut(ci) {
					if *c != ' ' {
						cs.push(*c);
					}
				}
			});

			i += 4;
			ci += 1;
		}
	});

	crate_stacks.iter_mut().for_each(|cs| { cs.pop(); cs.reverse(); });

	let instructions: Vec<(u16, u16, u16)> = instructions_str.split("\n")
		.map(|instr| {
			let mut instr_iter = instr.split(' ')
				.filter_map(|n| n.parse::<u16>().ok());
			
			let num = instr_iter.next();
			let from = instr_iter.next();
			let to = instr_iter.next();

			(num, from, to)
		})
		.filter(|(n, f, t)| n.is_some() && f.is_some() && t.is_some())
		.map(|(n, f, t)| (n.unwrap(), f.unwrap(), t.unwrap()))
		.collect();

	Ok((crate_stacks, instructions))
}

pub trait CrateStacks {
	fn num_stacks(&self) -> usize;
	fn top_each(&self) -> Vec<Option<char>>;
	fn move_crates(&mut self, num: u16, from: usize, to: usize);
	fn move_crates_at_once(&mut self, num: u16, from: usize, to: usize);
}

impl CrateStacks for Vec<Vec<char>> {

	fn num_stacks(&self) -> usize {
		self.len()
	}

	fn top_each(&self) -> Vec<Option<char>> {
		
		self.iter()
			.map(|stack| stack.last().map(|c| *c))
			.collect()
	}

	fn move_crates(&mut self, num: u16, from: usize, to: usize) {

		// ideally borrow before loop rather than on each iter
		for _ in 0..num {

			let c = self.get_mut(from).map(|f| f.pop());

			if let Some(Some(c)) = c {
				self.get_mut(to).map(|t| t.push(c));
			}
		}
	}

	fn move_crates_at_once(&mut self, num: u16, from: usize, to: usize) {

		let l = self.get(from).map(|f| f.len()).unwrap_or(0);

		if l == 0 { return }

		for _ in 0..num {

			let c = self.get_mut(from).map(|f| f.remove(l - (num) as usize));

			if let Some(c) = c {
				self.get_mut(to).map(|t| t.push(c));
			}
		}
	}
}