use std::collections::VecDeque;
use std::io::{Read, Result as IOResult};
use std::error::Error;
use std::str::FromStr;
use std::time::{Duration, Instant};

fn main() -> Result<(), Box<dyn Error>> {

	let input = get_input()?;

	let (p1_result, p1_time) = part1(input.clone())?;
	println!("got {} for part 1 in {}us", p1_result, p1_time.as_micros());

	let (p2_result, p2_time) = part2(input.clone())?;
	println!("got {} for part 2 in {}us", p2_result, p2_time.as_micros());

	Ok(())
}

fn get_input() -> IOResult<Vec<String>> {

	let mut buf = String::new();

	std::io::stdin().lock().read_to_string(&mut buf)?;

	Ok(buf.split('\n').map(|l| l.to_owned()).collect())
}

fn part1(input: Vec<String>) -> Result<(usize, Duration), Box<dyn Error>> {

	let start = Instant::now();

	let mut fsc: FileSystemCursor = FileSystem::new().into();

	let mut inputs: Vec<Input> = input.iter()
		.map(|i| Input::from_str(i))
		.filter_map(|i| i.ok())
		.collect();

	for input in inputs.drain(..) {

		fsc.process_input(input)?;
	}

	let filesystem: FileSystem = fsc.into();

	let mut sizes = vec![];

	dir_sizes(&filesystem, &mut sizes);

	let result: usize = sizes.iter()
		.map(|s| s.1.to_owned())
		.filter(|s| *s < 100_000)
		.sum();

	Ok((result, Instant::now() - start))
}

fn part2(input: Vec<String>) -> Result<(usize, Duration), Box<dyn Error>> {

	let start = Instant::now();

	let mut fsc: FileSystemCursor = FileSystem::new().into();

	let mut inputs: Vec<Input> = input.iter()
		.map(|i| Input::from_str(i))
		.filter_map(|i| i.ok())
		.collect();

	for input in inputs.drain(..) {
		
		fsc.process_input(input)?;
	}

	let filesystem: FileSystem = fsc.into();

	let mut sizes: Vec<(String, usize)> = vec![];
	dir_sizes(&filesystem, &mut sizes);

	sizes.sort_by(|a, b| a.1.cmp(&b.1));

	let free_space = 70_000_000 - filesystem.size();

	let sizes_sorted = sizes.iter_mut()
		.filter(|s| s.1 + free_space > 30_000_000 )
		.map(|s| s.to_owned())
		.collect::<Vec<(String, usize)>>();

	let result = sizes_sorted.first().map(|(_, size)| *size).unwrap_or(0);

	Ok((result, Instant::now() - start))
}


fn dir_sizes(fs: &FileSystem, dirs: &mut Vec<(String, usize)>) {

	match fs {
		FileSystem::Dir { name, contents } => {
			dirs.push((name.to_owned(), fs.size()));
			contents.iter().for_each(|f| dir_sizes(f, dirs));
		},
		_ => {}
	}
}

#[derive(Debug)]
pub struct ParseInputError;

impl Error for ParseInputError {}

impl std::fmt::Display for ParseInputError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "Failed to parse input")
	}
}

#[derive(Debug)]
pub enum Input {
	Cd(String),
	Ls,
	Dir(String),
	File(usize, String)
}

impl FromStr for Input {
	type Err = ParseInputError;
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		
		if let Some((first, second)) = s.split_once(' ') {

			if first == "$" {

				if let Some(("cd", dir)) = second.split_once(' ') {

					return Ok(Input::Cd(dir.to_owned()));
				} else if second == "ls" {

					return Ok(Input::Ls);
				}
			} else if first == "dir" {

				return Ok(Input::Dir(second.to_owned()));
			} else {

				if let Ok(size) = first.parse::<usize>() {
					return Ok(Input::File(size, second.to_owned()));
				}
			}
		}

		Err(ParseInputError)
		
	}
}

#[derive(Debug)]
pub enum FileSystem {
	File {
		name: String,
		size: usize
	},
	Dir {
		name: String,
		contents: Vec<FileSystem>
	}
}

#[derive(Debug)]
pub enum FileSystemError {
	FileNotFound(String),
	NotADir(String)
}

impl Error for FileSystemError {}

impl std::fmt::Display for FileSystemError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Self::FileNotFound(file) => write!(f, "File {} not found", file),
			Self::NotADir(file) => write!(f, "{} is not a dir", file)
		}
	}
}

impl FileSystem {

	pub fn new() -> FileSystem {
		Self::Dir { name: String::from(""), contents: vec![] }
	}

	pub fn name(&self) -> &str {
		match self {
			Self::File { name, .. } => name,
			Self::Dir { name, .. } => name
		}
	}

	pub fn size(&self) -> usize {

		match self {
			Self::File { size, .. } => *size,
			Self::Dir { contents, .. } => contents.iter().map(|fs| fs.size()).sum()
		}
	}

	pub fn insert<S: AsRef<str>>(&mut self, location: &mut VecDeque<String>, name: S, size: usize) -> Result<(), FileSystemError> {

		if let Some(d) = location.pop_front() {
			if let Some(fs) = self.get(&d) {
				fs.insert(location, name, size)
			} else {
				self.mkdir(location, &d)?;
				self.get(&d).unwrap().insert(location, name, size)
			}
		} else {
			match self {
				Self::File { name, .. } => Err(FileSystemError::NotADir(name.to_owned())),
				Self::Dir { contents, .. } => {
					contents.push(FileSystem::File { name: name.as_ref().to_owned(), size });
					Ok(())
				}
			}
		}
	}

	pub fn mkdir<S: AsRef<str>>(&mut self, location: &mut VecDeque<String>, dirname: S) -> Result<(), FileSystemError> {

		if let Some(d) = location.pop_front() {
			if let Some(fs) = self.get(&d) {
				fs.mkdir(location, dirname)
			} else {
				match self {
					Self::Dir { contents, .. } => {
						contents.push(Self::Dir { name: d.clone(), contents: vec![] });
						self.get(&d).unwrap().mkdir(location, dirname)
					},
					Self::File { name, .. } => Err(FileSystemError::NotADir(name.to_owned()))
				}
			}
		} else {

			match self {
				Self::Dir { contents, .. } => {
					contents.push(Self::Dir { name: dirname.as_ref().to_owned(), contents: vec![] });
					Ok(())
				},
				Self::File { name, .. } => Err(FileSystemError::NotADir(name.to_owned()))
			}
		}
	}

	pub fn get(&mut self, name: &str) -> Option<&mut Self> {

		match self {
			Self::File { .. } => None,
			Self::Dir { contents, .. } => {
				for d in contents {
					let n = match d {
						Self::File { name, .. } => name,
						Self::Dir { name, .. } => name
					};
					if name == n {
						return Some(d);
					}
				}
				None
			}
		}
	}
}


#[derive(Debug)]
pub struct FileSystemCursor {
	location: VecDeque<String>,
	fs: FileSystem
}

impl FileSystemCursor {

	pub fn cd<S: AsRef<str>>(&mut self, dir: S) -> Result<(), FileSystemError> {
		match &self.fs {
			FileSystem::File { name, .. } => Err(FileSystemError::NotADir(name.to_owned())),
			FileSystem::Dir { contents, .. } => {

				let dir = dir.as_ref();

				if dir == ".." {
					self.location.pop_back();

				} else {

					if contents.iter().find(|d| d.name() == dir).is_none() {
						self.mkdir(dir)?;
					}

					self.location.push_back(dir.to_owned());
				}

				Ok(())
			}
		}
	}

	pub fn mkdir<S: AsRef<str>>(&mut self, name: S) -> Result<(), FileSystemError> {

		self.fs.mkdir(&mut self.location.clone(), name)
	}

	pub fn mkfile<S: AsRef<str>>(&mut self, name: S, size: usize) -> Result<(), FileSystemError> {
		
		self.fs.insert(&mut self.location.clone(), name, size)
	}

	pub fn process_input(&mut self, input: Input) -> Result<(), FileSystemError> {

		match input {
			Input::Cd(dir) => self.cd(dir),
			Input::Ls => Ok(()),
			Input::Dir(name) => self.mkdir(name),
			Input::File(size, name) => self.mkfile(name, size)
		}
	}
}

impl Into<FileSystem> for FileSystemCursor {
	fn into(self) -> FileSystem {
		self.fs
	}
}

impl Into<FileSystemCursor> for FileSystem {
	fn into(self) -> FileSystemCursor {
		FileSystemCursor {
			location: VecDeque::new(),
			fs: self
		}
	}
}