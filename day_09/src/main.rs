use std::error::Error;
use std::fmt::Display;
use std::io::{Result as IOResult, Read};
use std::time::{Instant, Duration};
use std::collections::HashSet;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {

	let input = read_input()?;

	let (p1_result, p1_time) = part_1(input.clone())?;
	println!("got {} in {} microseconds for part 1", p1_result, p1_time.as_micros());

	let (p2_result, p2_time) = part_2(input.clone())?;
	println!("got {} in {} microseconds for part 2", p2_result, p2_time.as_micros());

	Ok(())
}

fn part_1(input: Vec<String>) -> Result<(usize, Duration), Box<dyn Error>> {

	let start = Instant::now();

	let instructions = input.iter()
		.filter_map(|l| Instruction::from_str(l).ok());
	
	let mut rope_bridge = RopeBridge::new(2);
	
	instructions.for_each(|instr| {
		rope_bridge.follow_instruction(&instr);

	});	
	
	Ok((rope_bridge.num_visited(), Instant::now() - start))
}

fn part_2(input: Vec<String>) -> Result<(usize, Duration), Box<dyn Error>> {
	
	let start = Instant::now();

	let instructions = input.iter()
		.filter_map(|l| Instruction::from_str(l).ok());
	
	let mut rope_bridge = RopeBridge::new(10);
	
	instructions.for_each(|instr| {
		rope_bridge.follow_instruction(&instr);
	});
		
	Ok((rope_bridge.num_visited(), Instant::now() - start))
}

fn read_input() -> IOResult<Vec<String>> {

	let mut buf = String::new();

	std::io::stdin().lock().read_to_string(&mut buf)?;

	Ok(buf.split('\n').map(|s| s.to_owned()).collect())
}

#[derive(Debug, Clone, Copy)]
enum Direction {
	Up,
	Right,
	Down,
	Left
}

// this might be useful for other challenges
macro_rules! simple_error {
	($t:ident, $e:expr) => {
		#[derive(Debug)]
		struct $t;

		impl std::error::Error for $t {}

		impl std::fmt::Display for $t {
			fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
				write!(f, "{}", $e)
			}
		}
	};
}

simple_error!(ParseInstructionError, "Failed to parse instruction");

struct Instruction {
	direction: Direction,
	distance: isize
}

impl FromStr for Instruction {
	type Err = ParseInstructionError; 
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let mut split = s.split(' ');

		let direction = match split.next()
			.ok_or(ParseInstructionError)?
			.chars()
			.next()
			.ok_or(ParseInstructionError)? {
			'U' => Ok(Direction::Up),
			'R' => Ok(Direction::Right),
			'D' => Ok(Direction::Down),
			'L' => Ok(Direction::Left),
			_ => Err(ParseInstructionError)
		}?;
		
		let distance = split.next()
			.map_or(Err(ParseInstructionError), |x| Ok(x.parse::<isize>().map_err(|_| ParseInstructionError))?)?;

		Ok(Self {
			direction,
			distance
		})
	}
}

impl Into<(isize, isize)> for Direction {
	fn into(self) -> (isize, isize) {
		match self {
			Self::Up    => (0, -1),
			Self::Right => (1, 0),
			Self::Down  => (0, 1),
			Self::Left  => (-1, 0)
		}
	}
}

#[derive(Debug)]
struct RopeBridge {
	rope: Vec<(isize, isize)>,
	visited: HashSet<(isize, isize)>
}

impl RopeBridge {
	
	pub fn new(rope_length: usize) -> Self {
		
		Self {
			rope: vec![(0, 0); rope_length],
			visited: HashSet::new()
		}
	}
	
	pub fn tail_position(&self) -> (isize, isize) {
		
		self.rope.last().unwrap_or(&(0, 0)).to_owned()
	}
	
	pub fn make_move(&mut self, instr: &Instruction) {
		
		let (dx, dy) = instr.direction.into();
		
		let mut pos_iter = self.rope.iter_mut();
		
		let mut previous_pos: Option<(isize, isize)> = None;
		
		while let Some(pos) = pos_iter.next() {
			
			if let Some(prev_pos) = previous_pos {
								
				let distance = (pos.0 - prev_pos.0).abs().max((pos.1 - prev_pos.1).abs());
				
				if distance > 1 {
					if pos.0 != prev_pos.0 && pos.1 != prev_pos.1 {
					
						let (mut mx, mut my) = (prev_pos.0 - pos.0, prev_pos.1 - pos.1);
						
						mx = if mx.abs() == 2 { mx/2 } else { mx };
						my = if my.abs() == 2 { my/2 } else { my };
						
						*pos = (
							pos.0 + mx,
							pos.1 + my
						)
						
					} else {
						*pos = (
							pos.0 + dx,
							pos.1 + dy
						)
					}
				}
				
				previous_pos = Some(*pos);
			} else {
				
				*pos = (
					pos.0 + dx,
					pos.1 + dy
				);
				
				previous_pos = Some(*pos);
			}
		}
		
		self.visited.insert(self.tail_position());
	}
	
	pub fn follow_instruction(&mut self, instr: &Instruction) {
		
		for _ in 0..instr.distance {
			
			self.make_move(instr);
		}
	}
	
	pub fn num_visited(&self) -> usize {
		
		self.visited.len()
	}
}

// to help with debugging
impl Display for RopeBridge {
	
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		
		let (mut min_x, mut min_y) = (-10, -10);
		let (mut max_x, mut max_y) = (10, 10);
		
		for &(x, y) in self.visited.iter().chain(self.rope.iter()) {
			
			if x < min_x { min_x = x }
			if y < min_y { min_y = y }
			if x > max_x { max_x = x }
			if y > max_y { max_y = y }
		}
		
		let mut output = String::new();
		
		for y in min_y..=max_y {
			for x in min_x..= max_x {
				
				let pos_index = self.rope.iter().position(|&i| i == (x, y));
				if let Some(i) = pos_index {
					if i == 0 {
						output.push('H');
					} else {
						output.push(('0' as u8 + i as u8) as char);
					}
				} else if self.visited.contains(&(x, y)) {
					output.push('#');
				} else if x == 0 && y == 0 {
					output.push('+');
				} else if x == 0 {
					output.push('|');
				} else if y == 0 {
					output.push('-');
				} else {
					output.push('.')
				}
			}
			
			output.push('\n');
		}
		
		write!(f, "{}", output)
	}
}